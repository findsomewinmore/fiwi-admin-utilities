<?php
/*
Plugin Name: Findsome & Winmore - Admin Utilities - ENV Email
Description: Disable Email Sending Staging Environments
Version: 3.6
Author: Vic Cao
Author URI: http://www.findsomewinmore.com/
GitHub Plugin URI: findsomewinmore/fiwi-admin-utilities
GitHub Plugin URI: https://bitbucket.org/findsomewinmore/fiwi-admin-utilities
*/


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
  'https://bitbucket.org/findsomewinmore/fiwi-admin-utilities',
  __FILE__,
  'fiwi-admin-utilities'
);

//Optional: Set the branch that contains the stable release.

$myUpdateChecker->setAuthentication(array(
  'consumer_key' => 'kvPtrYQ2cFY7BDZBBG',
  'consumer_secret' => 'spLx3m74PTqSK87wN3CV2QqSK5hvZ6mS',
));

$myUpdateChecker->setBranch('master');

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}


if(!function_exists('fiwi_disable_env_email')) {

function fiwi_disable_env_email() {


if(!get_option('env-email')){

$url = site_url();
if ((strpos($url,'staging') !== false)) {
define('DISABLE_EMAILS_PLUGIN_FILE', __FILE__);
define('DISABLE_EMAILS_PLUGIN_ROOT', dirname(__FILE__) . '/');
define('DISABLE_EMAILS_PLUGIN_NAME', basename(dirname(__FILE__)) . '/' . basename(__FILE__));
// options
define('DISABLE_EMAILS_OPTIONS', 'disable_emails');
include DISABLE_EMAILS_PLUGIN_ROOT . 'includes/class.DisableEmailsPlugin.php';
DisableEmailsPlugin::getInstance();
// replace standard WordPress wp_mail() if nobody else has already done it
if (!function_exists('wp_mail')) {
    function wp_mail( $to, $subject, $message, $headers = '', $attachments = array() ) {
        // create mock PHPMailer object to handle any filter and action hook listeners
        $mailer = new DisableEmailsPHPMailerMock();
        return $mailer->wpmail($to, $subject, $message, $headers, $attachments);
    }
    DisableEmailsPlugin::setActive();
}



function fiwi_disable_env_email_notice() {
    ?>
    <div class="notice notice-warning">
      <p>
        <?php _e( 'Note: Sending email has been disabled on this environment. To re-enable email <a href="/wp-admin/admin.php?page=fiwi-admin-env-email">click here</a>.', 'fiwi-disable-env-email' ); ?></p>
    </div>
    <?php
}
add_action( 'admin_notices', 'fiwi_disable_env_email_notice' );
}

}



function fiwi_disable_env_email_settings_page()
{
    add_settings_section("section", null, null, "fiwi_disable_env_email_settings");
    add_settings_field("env-email", "Enable ENV Email Sending", "fiwi_disable_env_email_checkbox_display", "fiwi_disable_env_email_settings", "section");
    register_setting("section", "env-email");
}

function fiwi_disable_env_email_checkbox_display()
{
   ?>
      <!-- Here we are comparing stored value with 1. Stored value is 1 if user checks the checkbox otherwise empty string. -->
      <input type="checkbox" name="env-email" value="1" <?php checked(1, get_option( 'env-email'), true); ?> />
      <?php
}

add_action("admin_init", "fiwi_disable_env_email_settings_page");

function fiwi_disable_env_email_page()
{
  ?>
        <div class="wrap">
          <h1>Disable FIWI ENV Email Override</h1>

          <form method="post" action="options.php">
            <?php
               settings_fields("section");

               do_settings_sections("fiwi_disable_env_email_settings");

               submit_button();
            ?>
          </form>
        </div>
        <?php
}

function fiwi_env_menu_menu_item()
{

    if (function_exists('acf_fields_admin')) {

      add_submenu_page(
          'admin-settings',
          __( 'ENV Email Override', 'textdomain' ),
          __( 'ENV Email Override', 'textdomain' ),
          'manage_options',
          'fiwi-admin-env-email',
          'fiwi_disable_env_email_page'
      );

    } else {

      add_submenu_page(
          'fiwi-admin-util',
          __( 'ENV Email Override', 'textdomain' ),
          __( 'ENV Email Override', 'textdomain' ),
          'manage_options',
          'fiwi-admin-env-email',
          'fiwi_disable_env_email_page'
      );


    }



}

add_action("admin_menu", "fiwi_env_menu_menu_item", 105);


}


add_action('init', 'fiwi_disable_env_email');



}
